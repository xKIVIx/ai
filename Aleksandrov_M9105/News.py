from sklearn.feature_extraction.text import CountVectorizer
import warnings
import numpy as np
import pandas as pd
import sklearn.model_selection as sk_ms
from sklearn.svm import LinearSVC


class News:
    def __init__(self):
        self.vectorizer = CountVectorizer(ngram_range=(0,2), 
                                 analyzer='word', 
                                 stop_words="english", 
                                 lowercase=True, 
                                 binary=True)
        self.classificator = LinearSVC()

        def fit(self, X, y):
            X = self.vectorizer.fit_transform(X)
            self.classificator.fit(X, y)

        def predict(self, X):
            X = self.vectorizer.transform(X)
            return self.classificator.predict(X)



