import numpy as np
import pandas as pd
import sklearn.model_selection as sk_ms
import sklearn.ensemble as sk_en
import sklearn.tree as sk_tree
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import PolynomialFeatures
from sklearn.feature_selection import SelectKBest



class Wine:
    def __int__(self):
        SEED = 1
        self.polynomialer = PolynomialFeatures(degree = 6)
        clr = [
            ('rf', sk_en.RandomForestClassifier(random_state = SEED)),
            ('dtc', sk_tree.DecisionTreeClassifier(random_state = SEED)),
            ('etc', sk_en.ExtraTreesClassifier(random_state = SEED, n_jobs = -1))
        ]
        self.classificator = sk_en.VotingClassifier(clr)

    def fit(self, X, y):
        y = y.map(lambda x: 0 if x <= 6.0 else 1)
        X = self.polynomialer.fit_transform(X)
        self.classificator.fit(X, y)

    def predict(self, X):
        X = self.polynomialer.transform(X)
        return  self.classificator.predict(X)




