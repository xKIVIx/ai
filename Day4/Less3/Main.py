import math

data:str = input()

data = data.split(" ")

data = list( map( int, filter(lambda x: x != "", data) ) )

result = "%i %i %i" % (math.gcd(data[0], data[1]),
                       math.gcd(data[0], data[2]),
                       math.gcd(data[1], data[2]))

print( result )