data:str = input()

data = data.split(" ")

data = list( filter(lambda x: x != "", data) )

data = sorted(data, key = lambda x: x[1])

result = ""

for i in data:
    result += i
    result += " "

print( result )
