class Time:
    # Конструктор, принимающий четыре целых числа: часы, минуты, секунды и миллисекунды.
    # В случае, если передан отрицательный параметр, вызвать исключение ValueError.
    # После конструирования, значения параметров времени должны быть корректными:
    # 0 <= GetHour() <= 23
    # 0 <= GetMinute() <= 59
    # 0 <= GetSecond() <= 59
    # 0 <= GetMillisecond() <= 999
    def __init__(self, hours=0, minutes=0, seconds=0, milliseconds=0):
        if((hours < 0) or (minutes < 0) or (seconds < 0) or (milliseconds < 0)):
            raise ValueError

        self._h = 0
        self._m = 0
        self._s = 0
        self._ml = 0

        self._AddHour(hours)
        self._AddMinets(minutes)
        self._AddSecond(seconds)
        self._AddMiliseconds(milliseconds)

    def _AddHour(self, hour):
        self._h += hour
        self._h %= 24

    def _AddMinets(self, minets):
        self._m += minets
        self._AddHour(int(self._m / 60))
        self._m %= 60

    def _AddSecond(self, seconds):
        self._s += seconds
        self._AddMinets(int(self._s / 60))
        self._s %= 60

    def _AddMiliseconds(self, miliseconds):
        self._ml += miliseconds
        self._AddSecond(int(self._ml / 1000))
        self._ml %= 1000

    def GetHour(self):
        return self._h

    def GetMinute(self):
        return self._m

    def GetSecond(self):
        return self._s

    def GetMillisecond(self):
        return self._ml
    # Прибавляет указанное количество времени к текущему объекту.
    # После выполнения этой операции параметры времени должны остаться корректными.

    def Add(self, time):
        self._AddHour(time._h)
        self._AddMinets(time._m)
        self._AddSecond(time._s)
        self._AddMiliseconds(time._ml)
    # Операторы str и repr должны представлять время в формате
    # HH:MM:SS.ms

    def __str__(self):
        r = ""
        if(self._h < 10):
            r += "0"
        r += str(self._h)
        r += ":"

        if(self._m < 10):
            r += "0"
        r += str(self._m)
        r += ":"

        if(self._s < 10):
            r += "0"
        r += str(self._s)

        r += "."
        r += str(self._ml)
        return r
