from functools import reduce

data:str = input()

data = data.split(" ")

data = list( map( int, filter(lambda x: x != "", data ) ) )

min_elem = reduce(lambda a,b: a if (a < b) else b, data)

max_elem = reduce(lambda a,b: a if (a > b) else b, data)

data = list( map( lambda x: x if(x != max_elem) else min_elem, data ) )

result = ""

for i in data:
    result += str( i )
    result += " "

print( result )


