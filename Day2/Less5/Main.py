count = int( input() )

for i in range( count ):
    for t in range( count ):
        print( "%i * %i = %i" % (i + 1, t + 1, ( i + 1 ) * ( t + 1 ) ) )

