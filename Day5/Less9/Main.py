import numpy as np

def GetData(valueType):
    inputData = input().split(" ")
    result = list(map(valueType, filter(lambda x: x!="", inputData)))
    return result


sizes = GetData(int)
mat = np.ones((sizes[0], sizes[0]), int)
for stringID in range(sizes[0]):
    data = GetData(int)
    mat[stringID, :] = np.array(data)

lmat = mat.T.tolist()

for i in range(sizes[0]):
    s = list(map(str, lmat[i]))
    print(" ".join(s))