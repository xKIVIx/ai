import numpy as np

def GetData(valueType):
    inputData = input().split(" ")
    result = list(map(valueType, filter(lambda x: x!="", inputData)))
    return result


sizes = GetData(int)
mat1 = np.ones( sizes[0], int)
mat2 = np.ones((sizes[0], 1), int)
for stringID in range(sizes[0]):
    data = GetData(int)
    mat1[stringID] = data[0]
    mat2[stringID, 0] = data[sizes[1] - 1]

print(np.dot(mat1, mat2)[0])