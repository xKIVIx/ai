import numpy as np
import pandas
import sys

file = pandas.read_csv("input.csv")
file.columns = ['Name', 'Size']
maxSize = file['Size'].max()
result = file[file['Size'] == maxSize]
for i in result['Name'].iloc[:]:
    print(i)
