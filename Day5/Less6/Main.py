import numpy as np
import sys

def GetData(valueType):
    inputData = input().split(" ")
    result = list(map(valueType, filter(lambda x: x!="", inputData)))
    return result


sizes = GetData(int)
mat = np.ones((sizes[0], sizes[1]), int)
for stringID in range(sizes[0]):
    data = GetData(int)
    mat[stringID, :] = np.array(data)

mat[[sizes[2], sizes[3]], :] = mat[[sizes[3], sizes[2]], :]

lmat = mat.tolist()

for i in range(sizes[0]):
    s = list(map(str, lmat[i]))
    print(" ".join(s))