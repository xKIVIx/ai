import numpy as np

inputData = input()
splitedData = inputData.split(" ")
clearData = list(map(float, filter(lambda x: x != "", splitedData)))
vector = np.array(clearData)

print(float(np.linalg.norm(vector, ord=2)))
