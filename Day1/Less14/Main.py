
def Count(A:int, B:int, t:int):
    P = A * 2 + B * 2
    rounds = int( t / P )
    lastWay = t - rounds * P

    if(lastWay < A):
        return "S"
    lastWay -= A

    if(lastWay < B):
        return "E"
    lastWay -= B

    if(lastWay < A):
        return "N"
    lastWay -= A

    return "W"
        


inputFile = open("input.txt", "r")
outputFile = open("output.txt", "w")

RawData:str = inputFile.readline()

data = RawData.split(" ")

result = Count( int( data[0] ), 
                int( data[1] ),
                int( data[2] ) )

outputFile.write( result )

