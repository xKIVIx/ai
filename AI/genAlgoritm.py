from joblib import Parallel, delayed

import numpy as np
import random
import sys

MUTATE_POWER = 5
COUNT_GENERATES = 100000
POPULATION_SIZE = 2000
LIFE_TIME = 100
SEED = 99942238

np.random.seed(SEED)
random.seed(SEED)

inputFile = open("input.txt", "r")
numTasks = int(inputFile.readline())
hardsTasks = np.array(list(map(int, inputFile.readline().split(" "))))
timesTasks = np.array(list(map(float, inputFile.readline().split(" "))))

numWorkers = int(inputFile.readline())
r = ""
for i in range(numWorkers):
    r += inputFile.readline().replace("\n", " ")
r = r.split(" ")
r = map(float, filter(lambda x: x != "", r))
koefWorkers = np.array(list(r))
koefWorkers.shape = (numWorkers, 4)


hardsTasks -= 1
hardMatrix = np.ones((numWorkers, numTasks))
hardMatrix *= hardsTasks
for i in range(numWorkers):
    hardMatrix[i, :] = koefWorkers[i, hardsTasks]

timeMatrix = hardMatrix[range(numWorkers), :] * timesTasks

class Generate():

    def __init__(self, board=None):
        self.lifeTime = 0
        if board is None:
            self.NewGen()
        else:
            self.workersTasks = board

        self.ComputeCompliteTime()
        
    def NewGen(self):
        self.workersTasks = np.random.randint(numWorkers, size = numTasks)

    def Iteration(self):
        self.lifeTime += 1
        if self.lifeTime > LIFE_TIME:
            self.NewGen()
            self.ComputeCompliteTime()

    def __ge__(self, value):
        return self.compliteTime >= value.compliteTime

    def __gt__(self, value):
        return self.compliteTime > value.compliteTime

    def __le__(self, value):
        return self.compliteTime <= value.compliteTime

    def __lt__(self, value):
        return self.compliteTime < value.compliteTime

    def __eq__(self, value):
        return self.compliteTime == value.compliteTime

    def __str__(self):
        result = self.workersTasks + 1
        result = result.tolist()
        result = list(map(str, result))
        return " ".join(result)

    def ComputeCompliteTime(self):
        resultMat = np.zeros((numWorkers, numTasks))
        resultMat[self.workersTasks, range(numTasks)] = timeMatrix[self.workersTasks, range(numTasks)]
        resultMat = resultMat.sum(axis = 1)
        self.compliteTime = resultMat.max()

    def GetCompliteTime(self):
        return self.compliteTime

    def Mutate(self):
        result = np.copy(self.workersTasks)
        ids = np.random.randint(numTasks-1, size = MUTATE_POWER)
        result[ids] = random.randint(0, numWorkers - 1)
        return Generate(result)

    def MakeChild(self, two):
        #unionMatrix = np.empty((2, numTasks), int)
        #unionMatrix[0, :] = self.workersTasks
        #unionMatrix[1, :] = two.workersTasks


        #ids = np.random.randint(0, 2, numTasks)

        #result = np.empty(numTasks, int)
        result = self.workersTasks.copy()
        result[:int(numTasks/2)] = two.workersTasks[:int(numTasks/2)]

        self.Iteration()
        two.Iteration()
        return Generate(result)


def Mutate(a):
    return a.Mutate()


vMutate = np.vectorize(Mutate)


def MakeChild(a, b):
    return a.MakeChild(b)

vMakeChild = np.vectorize(MakeChild)


population = np.empty(POPULATION_SIZE, dtype=Generate)
for i in range(POPULATION_SIZE):
    population[i] = Generate()

minimalLast = 900
for i in range(COUNT_GENERATES):
    newPopulation = vMakeChild(population[:int(population.size/2)],
                               population[int(population.size/2):])
    newPopulation = np.append(newPopulation, vMutate(newPopulation))
    newPopMin = newPopulation.min().GetCompliteTime()
    population = np.fmin(population, newPopulation)
    minGen = population.min()
    minimalCurrent = minGen.GetCompliteTime()
    np.random.shuffle(population)
    print("Turn : %i/%i - minimal current : %f minimal new pop: %f"%(i, COUNT_GENERATES, minimalCurrent, newPopMin))
    if minimalCurrent < minimalLast:
        minimalLast = minimalCurrent
        print("New minimal: %f"%(minimalLast))
        fileOut = open("output.txt", "w")
        fileOut.write(str(minGen))
        fileOut.close()