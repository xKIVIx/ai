import numpy as np

# h(x)
def linear_func(theta, X):
    return X.dot(theta)

def logistic_func(theta, x):
    return logistic_func_all(theta, x)

def logistic_func_all(theta, X):
    result = linear_func(theta, X)
    return 1/(1 + np.exp(-result))

def cross_entropy_loss(theta, X, y):
	y_calc = linear_func(theta, X)
	result = -y * y_calc
	result = 1.0 + np.exp(result)
	result = np.log(result)
	return result.mean()

def grad_cross_entropy_loss(theta, X, y):
    y_calc = linear_func(theta, X)
    k = - y / ( 1.0 + np.exp(y * y_calc))
    result = np.tile(k, (X.shape[1], 1)) 
    return np.mean(result.T * X, axis=0)

def fit_logistic_regression(X, y):
    a = 1e16
    theta = np.ones(X.shape[1])
    while cross_entropy_loss(theta, X, y) > 0.000001:
        gr = grad_cross_entropy_loss(theta, X, y);
        theta -= a * gr
    return theta
