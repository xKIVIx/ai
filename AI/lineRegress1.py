import numpy as np
#import matplotlib.pyplot as plt

def linear_func(theta, x):
    return linear_func_all(theta, x)



def linear_func_all(theta, X):
    return X.dot(theta)


def mean_squared_error(theta, X, y):
    h = linear_func_all(theta, X)
    result = y - h
    result **= 2.0
    return result.sum() / float(result.shape[0])

def grad_mean_squared_error(theta, X, y):
    # расчитать значение по linear_func_all
    y_f = linear_func_all(theta, X)
    # вычислить разницу действительных значений и полученых в результате функции
    dy = y_f - y
    # умножить матрицы разницы и X
    dy = dy.dot(X)
    # умножить на 2
    dy *= 2
    # разделить на число строк X
    dy /= float(X.shape[0])
    return dy

def fit_linear_regression(X, y):
    theta = np.ones_like(X[0,:])
    alpha = 1e15
    grad = grad_mean_squared_error(theta, X, y)
    er = mean_squared_error(theta, X, y)
    while er > 0.0001:
        theta -= grad * alpha
        grad = grad_mean_squared_error(theta, X, y)
        er = mean_squared_error(theta, X, y)
    #g = plt.plot(er)
    #plt.show()

    return theta
